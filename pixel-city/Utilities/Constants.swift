//
//  Constants.swift
//  pixel-city
//
//  Created by cjnora on 2018/2/28.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import Foundation

let apiKey = "6c3c3cbd8d4354da4ee8a9d97cf2319d"

func flickrUrl(forApiKey key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(key)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&extras=description%2Ctags%2Cdate_taken%2Cdate_upload%2C%2Ccount_comment%2Ccount_faves%2Cviews%2Cgeo&per_page=\(number)&format=json&nojsoncallback=1"
    print(url)
    return url
//    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}


