//
//  PhotoCell.swift
//  pixel-city
//
//  Created by cjnora on 2018/2/19.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
