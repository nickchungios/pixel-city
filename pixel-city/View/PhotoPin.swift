//
//  PhotoPin.swift
//  pixel-city
//
//  Created by cjnora on 2018/3/4.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import Foundation
import MapKit

class PhotoPin: NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        super.init()
    }
    
}
