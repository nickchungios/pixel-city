//
//  PopVC.swift
//  pixel-city
//
//  Created by cjnora on 2018/2/28.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit
import MapKit

class PopVC: UIViewController,UIGestureRecognizerDelegate {
    
    //Outlets
    @IBOutlet weak var popImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    //Variables
    var passedImage: UIImage!
    var url:URL?
    var imgTitle:String?
    var imgContent:String?
    var imgDate:String?
    var imageData:Data?
    let regionRadius:Double = 1000
    var photoPin:PhotoPin?
    
    //Set up
    func initData(forImage image:UIImage,forTitle title:String,forContent content:String,forDate date:String,forPin pin:PhotoPin) {
        self.passedImage = image
        self.imgTitle = title
        self.imgContent = content
        self.imgDate = date
        self.photoPin = pin
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        popImageView.image = passedImage
        titleLbl.text = imgTitle
        contentLbl.text = imgContent
        dateLbl.text = imgDate
        addDoubletap()
        setupMap()
    }
    
    //Functions
    func addDoubletap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTapped))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    
    @objc func screenWasDoubleTapped() {
        self.dismiss(animated: true, completion: nil)
    }

}

extension PopVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pinAnnotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "photoPin")
        pinAnnotation.pinTintColor = #colorLiteral(red: 0.9771530032, green: 0.7062081099, blue: 0.1748393774, alpha: 1)
        pinAnnotation.animatesDrop = true
        return pinAnnotation
    }
    
    func centerMapOnLoccation() {
        guard let coordinate = photoPin?.coordinate else { return }
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func setupMap() {
        let annotation = photoPin
        mapView.addAnnotation(annotation!)
        centerMapOnLoccation()
    }
}
